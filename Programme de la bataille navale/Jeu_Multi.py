# -*- coding: utf-8 -*-
import os
os.system("clear")
        # Couleurs
class color:
    BOLD = "\033[1m"
    YELLOW = "\033[93m"
    END = "\033[0m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    PURPLE = "\033[95m"
    
def affichageMenu():
    pass
    

def ReseauServ():
    import socket
    import select
    
    hote = ''
    port = 12800
    
    connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connexion_principale.bind((hote, port))
    connexion_principale.listen(5)
    print("Le serveur écoute à présent sur le port {}".format(port))
    
    serveur_lance = True
    clients_connectes = []
    while serveur_lance:
        connexions_demandees, wlist, xlist = select.select([connexion_principale],
            [], [], 0.05)
        
        for connexion in connexions_demandees:
            connexion_avec_client, infos_connexion = connexion.accept()
    
            clients_connectes.append(connexion_avec_client)
        
        clients_a_lire = []
        try:
            clients_a_lire, wlist, xlist = select.select(clients_connectes,
                    [], [], 0.05)
        except select.error:
            pass
        else:
            for client in clients_a_lire:
                msg_recu = client.recv(1024)
    
                msg_recu = msg_recu.decode()
                print("Reçu : {}".format(msg_recu))
                answer = input("> ")
                answer = answer.encode()
                client.send(answer)
                if msg_recu == "fin":
                    serveur_lance = False
    
    print("Fermeture des connexions")
    for client in clients_connectes:
        client.close()
    
    connexion_principale.close()
    
    
def ReseauClient():
    import socket

    hote = input("Entrer l'adresse IP de votre adversaire : ")
    port = 12800
    
    connexion_avec_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connexion_avec_serveur.connect((hote, port))
    print("Connexion établie avec le serveur sur le port {}".format(port))
    
    msg_a_envoyer = b""
    while msg_a_envoyer != b"fin":
        msg_a_envoyer = input("> ")
        # Peut planter si vous tapez des caractères spéciaux
        msg_a_envoyer = msg_a_envoyer.encode()
        # On envoie le message
        connexion_avec_serveur.send(msg_a_envoyer)
        msg_recu = connexion_avec_serveur.recv(1024)
        print(msg_recu.decode()) # Là encore, peut planter s'il y a des accents
    
    print("Fermeture de la connexion")
    connexion_avec_serveur.close()


    
def PartieSolo():
    import os
    os.system("clear")
    print("Entrer un intervalle : ")
    Intervalle=int(input("L'intervalle va de 1 à : "))
    import random
    BateauX = random.randint(1,Intervalle)
    import random
    BateauY = random.randint(1,Intervalle)
    munition=int(input("Choisissez un nombre de munitions max : "))
    x=0
    y=0
    while munition!=0 and (x!=BateauX or y!=BateauY):
        print()
        print("        A vous de jouer soldat !")
        x=int(input("Abscisse x de votre tir : "))
        y=int(input("Ordonnée y de votre tir : "))
        print()
        if (x>0 and x<=Intervalle) and (y>0 and y<=Intervalle):
            if munition==0 :
                print("Vous n'avez plus de munitions ! La mission est un échec.")
            if x==BateauX and y==BateauY:
                print(color.GREEN + color.BOLD + "Navire touché !" + color.END)
                print("La mission est un succès !")
            else:
                if BateauX-1<=x<=BateauX+1 and BateauY-1<=y<=BateauY+1:
                    print(color.YELLOW + color.BOLD + "Pas loin !" + color.END)
                else:
                    print(color.RED + color.BOLD + "Raté !" + color.END)
                munition=munition-1
                print(color.PURPLE + "Nombre de tirs restant :", munition, color.END)
            if munition==0 :
                print("Vous n'avez plus de munitions ! La mission est un échec.")
        else:
            print(color.PURPLE + "ATTENTION : Vos coordonnées doivent appartenir à l'intervalle allant de 1 à" , Intervalle, ".", color.END)
    print("Rentrez à la base.")
    choix="0"
    while choix !="q":
        choix=input(color.RED + color.BOLD + 'Entrer "q" pour revenir au menu principal. ' + color.END)
        pass
    import os
    os.system("clear")
    print('Bienvenue dans BattleShip: Seaforce !')
    print()
    print('    | Entrer "s" pour lancer une partie solo.')
    print('    | Entrer "m" pour créer une partie multijoueur.')
    print('    | Entrer "r" pour rejoindre une partie multijoueur.')
    print()
    print('  Entrer "q" pour quitter le jeu')
    print()


def PartieLocale():
    
    tab1=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
    for i in range(10) :
        for j in range(10) :
            print(tab1[i][j], end=" ")
            print()                 # Grille de jeu
    
    tab2=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
    for i in range(10) :
        for j in range(10) :
            print(tab2[i][j], end=" ")
        print()                 # Grille de jeu
    
    tab3=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
    for i in range(10) :
        for j in range(10) :
            print(tab3[i][j], end=" ")
        print()                 # Grille de jeu
    
    tab4=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
    for i in range(10) :
        for j in range(10) :
            print(tab4[i][j], end=" ")
        print()                 # Grille de jeu

    # Bateau du joueur 1
    import os
    os.system("clear")
    print("Joueur 1 place son bateau.")
    BateauX1 = int(input('Abscisse de votre navire : '))-1
    BateauY1 = int(input('Ordonnée de votre navire : '))-1
    print()
    tab1[BateauY1][BateauX1]='0'
    for i in range(10) :
        for j in range(10) :
            print(tab1[i][j], end=" ")
        print()                 # Grille affichant le bateau du joueur 1
        
    # Bateau du joueur 2
    import os
    os.system("clear")
    print("Joueur 2 place son bateau.")
    BateauX2 = int(input('Abscisse de votre navire : '))-1
    BateauY2 = int(input('Ordonnée de votre navire : '))-1
    print()
    tab2[BateauY2][BateauX2]='0'
    for i in range(10) :
        for j in range(10) :
            print(tab2[i][j], end=" ")
        print()                 # Grille affichant le bateau du joueur 2

    # Tir joueurs
    x1=0.5
    y1=0.5
    x2=0.5
    y2=0.5
    import os
    os.system("clear")
    while (x1!=BateauX2 or y1!=BateauY2) and (x2!=BateauX1 or y2!=BateauY1) :    
    
            # Tir joueur 1
        print("Au tour du joueur 1.")
        x1=int(input("Abscisse de votre tir : "))-1
        y1=int(input("Ordonnée de votre tir : "))-1
        print()
        
        while not ((x1>=0 and x1<=10) and (y1>=0 and y1<=10)):
            print("Votre tir n'appartient pas à la zone de jeu. Rejouez !")
            print("Au tour du joueur 1.")
            x1=int(input("Abscisse de votre tir : "))-1
            y1=int(input("Ordonnée de votre tir : "))-1
            print()
            
        if x1==BateauX2 and y1==BateauY2 :
            print("Navire touché !")
            tab4[y1][x1]='¤'
            for i in range(10) :
                for j in range(10) :
                    print(tab4[i][j], end=" ")
                print()                 # Grille de jeu
        else:
            if BateauX2-1<=x1<=BateauX2+1 and BateauY2-1<=y1<=BateauY2+1:
                print("En vue !")
                tab4[y1][x1]='?'
                for i in range(10) :
                    for j in range(10) :
                        print(tab4[i][j], end=" ")
                    print()                 # Grille de jeu
            else:
                print("Raté !")
                tab4[y1][x1]='#'
                for i in range(10) :
                    for j in range(10) :
                        print(tab4[i][j], end=" ")
                    print()                 # Grille de jeu    
        
            # Tir du joueur 2
        print("Au tour du joueur 2.")
        x2=int(input("Abscisse de votre tir : "))-1
        y2=int(input("Ordonnée de votre tir : "))-1
        print()
        
        while not ((x2>=0 and x2<=10) and (y2>=0 and y2<=10)):
            print("Votre tir n'appartient pas à la zone de jeu. Rejouez !")
            print("Au tour du joueur 2.")
            x2=int(input("Abscisse de votre tir : "))-1
            y2=int(input("Ordonnée de votre tir : "))-1
            print()
            
        if x2==BateauX1 and y2==BateauY1 :
            print("Navire touché !")
            tab3[y2][x2]='¤'
            for i in range(10) :
                for j in range(10) :
                    print(tab3[i][j], end=" ")
                print()                 # Grille de jeu
        else:
            if BateauX1-1<=x2<=BateauX1+1 and BateauY1-1<=y2<=BateauY1+1:
                print("En vue !")
                tab3[y2][x2]='?'
                for i in range(10) :
                    for j in range(10) :
                        print(tab3[i][j], end=" ")
                    print()                 # Grille de jeu
            else:
                print("Raté !")
                tab3[y2][x2]='#'
                for i in range(10) :
                    for j in range(10) :
                        print(tab3[i][j], end=" ")
                    print()                 # Grille de jeu  

    print("La partie est terminée !")
    choix="0"
    while choix !="q":
        choix=input(color.RED + color.BOLD + 'Entrer "q" pour revenir au menu principal. ' + color.END)
        pass
    import os
    os.system("clear")
    print('Bienvenue dans BattleShip: Seaforce !')
    print()
    print('    | Entrer "s" pour lancer une partie solo.')
    print('    | Entrer "m" pour créer une partie multijoueur.')
    print('    | Entrer "r" pour rejoindre une partie multijoueur.')
    print()
    print('  Entrer "q" pour quitter le jeu')
    print()

    
def CreerMulti():
    ReseauServ()

def RejoindreMulti():
    pass

            # IHM
print('Bienvenue dans BattleShip: Seaforce !')
print()
print('    | Entrer "s" pour lancer une partie solo.')
print('    | Entrer "l" pour jouer en multijoueur sur le même PC.')
print('    | Entrer "m" pour créer une partie multijoueur.')
print('    | Entrer "r" pour rejoindre une partie multijoueur.')
print()
print('  Entrer "q" pour quitter le jeu')
print()
choix="0"
while choix != "q":
    affichageMenu()
    choix=input(color.RED + color.BOLD + ' Choisissez une action : ' + color.END)
    print()
    if choix=="s":
        PartieSolo()
    elif choix=="l":
        PartieLocale()
    elif choix =="m":
        CreerMulti()
    elif choix =="r":
        RejoindreMulti()
    else:
        pass