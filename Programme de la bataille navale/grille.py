# -*- coding: utf-8 -*
tab1=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
for i in range(10) :
    for j in range(10) :
        print(tab1[i][j], end=" ")
    print()                 # Grille de jeu
    
tab2=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
for i in range(10) :
    for j in range(10) :
        print(tab2[i][j], end=" ")
    print()                 # Grille de jeu
    
tab3=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
for i in range(10) :
    for j in range(10) :
        print(tab3[i][j], end=" ")
    print()                 # Grille de jeu
    
tab4=[['~' for nbrcolonne in range(10)] for nbrligne in range(10)]
for i in range(10) :
    for j in range(10) :
        print(tab4[i][j], end=" ")
    print()                 # Grille de jeu

    # Bateau du joueur 1
import os
os.system("clear")
print("Joueur 1 place son bateau.")
BateauX1 = int(input('Abscisse de votre navire : '))-1
BateauY1 = int(input('Ordonnée de votre navire : '))-1
print()
tab1[BateauY1][BateauX1]='0'
for i in range(10) :
    for j in range(10) :
        print(tab1[i][j], end=" ")
    print()                 # Grille affichant le bateau du joueur 1
        
    # Bateau du joueur 2
import os
os.system("clear")
print("Joueur 2 place son bateau.")
BateauX2 = int(input('Abscisse de votre navire : '))-1
BateauY2 = int(input('Ordonnée de votre navire : '))-1
print()
tab2[BateauY2][BateauX2]='0'
for i in range(10) :
    for j in range(10) :
        print(tab2[i][j], end=" ")
    print()                 # Grille affichant le bateau du joueur 2

    # Tir joueurs
x1=0.5
y1=0.5
x2=0.5
y2=0.5
import os
os.system("clear")
while (x1!=BateauX2 or y1!=BateauY2) and (x2!=BateauX1 or y2!=BateauY1) :    
    
    # Tir joueur 1
    print("Au tour du joueur 1.")
    x1=int(input("Abscisse de votre tir : "))-1
    y1=int(input("Ordonnée de votre tir : "))-1
    print()
    while not ((x1>=0 and x1<=10) and (y1>=0 and y1<=10)):
        if x1==BateauX2 and y1==BateauY2 :
            print("Navire touché !")
            tab4[y1][x1]='¤'
            for i in range(10) :
                for j in range(10) :
                    print(tab4[i][j], end=" ")
                print()                 # Grille de jeu
        else:
            if BateauX2-1<=x1<=BateauX2+1 and BateauY2-1<=y1<=BateauY2+1:
                print("En vue !")
                tab4[y1][x1]='?'
                for i in range(10) :
                    for j in range(10) :
                        print(tab4[i][j], end=" ")
                    print()                 # Grille de jeu
            else:
                print("Raté !")
                tab4[y1][x1]='#'
                for i in range(10) :
                    for j in range(10) :
                        print(tab4[i][j], end=" ")
                    print()                 # Grille de jeu    
    else:
        print("Votre tir n'appartient pas à la zone de jeu. Rejouez !")
        
    # Tir du joueur 2
    print("Au tour du joueur 2.")
    x2=int(input("Abscisse de votre tir : "))-1
    y2=int(input("Ordonnée de votre tir : "))-1
    print()
    while not ((x2>=0 and x2<=10) and (y2>=0 and y2<=10)):
        if x2==BateauX1 and y2==BateauY1 :
            print("Navire touché !")
            tab3[y2][x2]='¤'
            for i in range(10) :
                for j in range(10) :
                    print(tab3[i][j], end=" ")
                print()                 # Grille de jeu
        else:
            if BateauX1-1<=x2<=BateauX1+1 and BateauY1-1<=y2<=BateauY1+1:
                print("En vue !")
                tab3[y2][x2]='?'
                for i in range(10) :
                    for j in range(10) :
                        print(tab3[i][j], end=" ")
                    print()                 # Grille de jeu
            else:
                print("Raté !")
                tab3[y2][x2]='#'
                for i in range(10) :
                    for j in range(10) :
                        print(tab3[i][j], end=" ")
                    print()                 # Grille de jeu  
    else:
        print("Votre tir n'appartient pas à la zone de jeu. Rejouez !")

print("La partie est terminée !")

    