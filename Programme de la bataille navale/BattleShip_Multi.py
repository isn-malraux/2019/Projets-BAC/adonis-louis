# -*- coding: utf-8 -*-
joueur1=str(input('Soldat n°1, entrez votre pseudo :'))
print()
joueur2=str(input('Soldat n°2, entrez votre pseudo :'))
print()
print("Entrer un intervalle :")
Intervalle=int(input("L'intervalle va de 1 à :"))
munition=int(input("Choisissez un nombre de munitions max :"))
munition1=munition
munition2=munition
print()
print(joueur1, ('entrez les coordonées de votre navire.'))
print()
BateauX1 = int(input('Abscisse de votre navire :'))
BateauY1 = int(input('Ordonnée de votre navire :'))
print()
print(joueur2, ('entrez les coordonées de votre navire.'))
print()
BateauX2 = int(input('Abscisse de votre navire :'))
BateauY2 = int(input('Ordonnée de votre navire :'))
x1=0
y1=0
x2=0
y2=0
class color:
    BOLD = "\033[1m"
    YELLOW = "\033[93m"
    END = "\033[0m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    PURPLE = "\033[95m"
while munition1!=0 and (x1!=BateauX2 or y1!=BateauY2) and munition2!=0 and (x2!=BateauX1 or y2!=BateauY1):
    print()
    print("        A vous de jouer", joueur1,'!')
    x1=int(input("Abscisse de votre tir :"))
    y1=int(input("Ordonnée de votre tir :"))
    print()
    if (x1>0 and x1<=Intervalle) and (y1>0 and y1<=Intervalle):
        if munition==0 :
            print("Vous n'avez plus de munitions !", joueur2, 'gagne !')
        if x1==BateauX2 and y1==BateauY2:
            print(color.GREEN + color.BOLD + "Navire touché !" + color.END)
            print("La mission est un succès !")
        else:
            if BateauX2-1<=x1<=BateauX2+1 and BateauY2-1<=y1<=BateauY2+1:
                print(color.YELLOW + color.BOLD + "Pas loin !" + color.END)
            else:
                print(color.RED + color.BOLD + "Raté !" + color.END)
            munition=munition-1
            print(color.PURPLE + "Nombre de tirs restant :", munition, color.END)
        if munition==0 :
            print("Vous n'avez plus de munitions ! La mission est un échec.")
    else:
        print(color.PURPLE + "ATTENTION : Vos coordonnées doivent appartenir à l'intervalle allant de 1 à" , Intervalle, ".", color.END)
print("Rentrez à la base.")