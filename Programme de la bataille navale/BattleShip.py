# -*- coding: utf-8 -*-
print("Entrer un intervalle :")
Intervalle=int(input("L'intervalle va de 1 à :"))
import random
BateauX = random.randint(1,Intervalle)
import random
BateauY = random.randint(1,Intervalle)
munition=int(input("Choisissez un nombre de munitions max :"))
x=0
y=0
class color:
    BOLD = "\033[1m"
    YELLOW = "\033[93m"
    END = "\033[0m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    PURPLE = "\033[95m"
while munition!=0 and (x!=BateauX or y!=BateauY):
    print()
    print("        A vous de jouer soldat !")
    x=int(input("Abscisse x de votre tir :"))
    y=int(input("Ordonnée y de votre tir :"))
    print()
    if (x>0 and x<=Intervalle) and (y>0 and y<=Intervalle):
        if munition==0 :
            print("Vous n'avez plus de munitions ! La mission est un échec.")
        if x==BateauX and y==BateauY:
            print(color.GREEN + color.BOLD + "Navire touché !" + color.END)
            print("La mission est un succès !")
        else:
            if BateauX-1<=x<=BateauX+1 and BateauY-1<=y<=BateauY+1:
                print(color.YELLOW + color.BOLD + "Pas loin !" + color.END)
            else:
                print(color.RED + color.BOLD + "Raté !" + color.END)
            munition=munition-1
            print(color.PURPLE + "Nombre de tirs restant :", munition, color.END)
        if munition==0 :
            print("Vous n'avez plus de munitions ! La mission est un échec.")
    else:
        print(color.PURPLE + "ATTENTION : Vos coordonnées doivent appartenir à l'intervalle allant de 1 à" , Intervalle, ".", color.END)
print("Rentrez à la base.")