### **Adonis-Louis**

Projet de BAC Spécialité ISN session 2019 : Bataille navale

## -                                      **Cahier des charges**                                                              

# ***Contexte***
Élaboration d’un programme en vu du BAC d’ISN 2019.

# ***Projet***                                                                        
Cette version originale de la bataille navale consiste en l’affrontement de deux adversaires dans une zone maritime représentée par un tableau. Il y aura plusieurs classes de bateaux disponibles :						                           
	- Destroyer (deux tirs par tour -2 vies) -capacité (optionnelle) : tir de 4 cases                    
	- Porte-avions (un tir par tour -4 vies) -capacité (optionnelle) : libère un avion                                  

 Il sera possible de jouer au jeu en multijoueur (réseau) ou seul contre un bateau placé aléatoirement sur la zone de jeu constituée de :       
 "~" symbolisant l'eau                                                                
 "#" symbolisant un tir échoué                                                                         
 "¤" symbolisant un tir touché                                                                           
 "?" symbolisant "en vue"     
 "0" symbolisant son bateau
Le codage du jeu sera fait en Python3.

L’objectif global est de fournir un jeu fonctionnel en multijoueur au format initial en Python. 

Une interface graphique basique pourrait être mise en place.

Le programme doit être libre, il doit offrir les libertés suivantes :
- copie
- analyse
- modification
- rediffusion des modifications
Le programme ne peut pas devenir non-libre.

On pourra s’aider de plusieurs bibliothèques python.

1 Battleship: Seaforce                                                                
	1.1 Multijoueur                                                                  
		1.1.1 Tour par tour                                                              
			1.1.1.1 Envoie d'une grille de jeu à chaques tours                           
	1.2 Menu principale                                                                        
		1.2.1 Solo                                                                    
			1.2.1.1 Pas de choix de classe                                                 
		1.2.2 Multijoueur                                                           
			1.2.2.1 Entrer l'IP du joueur 2                                            
			1.2.2.2 Choix des classes                                                        
				1.2.2.2.1 Destroyer                                                      
				1.2.2.2.2 Porte-avions                                                         
	1.3 Grille de jeu:                                                                     
		1.3.1 Grille de jeu de 10x10                                                     
		1.3.2 "#": tir loupé                                                               
		1.3.3 "?": En vue                                                              
		1.3.4 "¤": Touché                                                                
		1.3.5 "~": A l'eau    
		1.3.6 "0": Son bateau
	1.4 Destroyer                                                                   
		1.4.1 : 2 tir par tour                                                          
			1.4.1.1 : 1 case                                                            
		1.4.2 : 2 vies                                                                  
		1.4.3 Capacité spéciale                                                       
			1.4.3.1 : tir de 4 cases                                                         
	1.5 Interface graphique                                                          
		1.5.1 sélection simple des options                                              
		1.5.2 Image de présentation en arrière plan                                                 
	1.6 Quitter                                                                       
		1.6.1 Fin du programme                                                          
	1.7 Porte-avions                                                                      
		1.7.1 Capacité spéciale                                                          
			1.7.1.1 Libérer un avion                                                          
		1.7.2 : 1 tir par tour                                                      
			1.7.2.1 : 1 case                                                             
		1.7.3 : 4 vies                                                                   